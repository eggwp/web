<matches>
  <h1>Upcoming matches</h1>
  <p each={id, i in opts.matches} key={id}>
    you ({opts.teams[opts.profile.team].name}) vs {opts.teams[id].name}
  </p>

  <script>
    import { equals } from 'ramda'
    
    this.shouldUpdate = (data, nextOpts) => !equals(this.opts, nextOpts)
  </script>
</matches>
