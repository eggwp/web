<ranking-table>
  <div class="ranking-table">
    <div class="col">
      <div class="row">Name</div>
      <div class="row" each={value, name in opts.teams}><a href="/team/{name}">{value.name}</a></div>
    </div>
    <div class="col">
      <div class="row">Points</div>
      <div class="row" each={value, name in opts.teams}>{value.points}</div>
    </div>
    <div class="col">
      <div class="row">Streak</div>
      <div class="row" each={value, name in opts.teams}>{value.streak}</div>
    </div>
  </div>

  <script>
    import { equals } from 'ramda'
    import { updateTeamAction } from '../../store/entities/teams/actions'

    this.shouldUpdate = (data, nextOpts) => !equals(this.opts, nextOpts)
  </script>
</ranking-table>
