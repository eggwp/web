<challenges>
  <h1>Challenges</h1>
  <p each={id, i in opts.challenges} key={id}>
    you ({opts.teams[opts.profile.team].name}) vs {opts.teams[id].name}
    <button onclick={() => dealWithChallenge(id, true)}>Accept</button>
    <button onclick={() => dealWithChallenge(id, false)}>Decline</button>
  </p>

  <script>
    import { equals } from 'ramda'
    import { acceptDeclineChallenge } from '../../store/entities/profile/actions'

    this.shouldUpdate = ((data, nextOpts) => !equals(this.opts, nextOpts))

    this.dealWithChallenge = (teamId, accept) => acceptDeclineChallenge(teamId, accept)
  </script>
</challenges>
