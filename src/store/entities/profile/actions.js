import { profileStream } from './stream'

export function updateProfile(response) {
  return profileStream.push(['UPDATE_PROFILE', response])
}

export function challengeTeam(challengedId, myId) {
  return profileStream.push(['CHALLENGE_TEAM', challengedId])
}

export function acceptDeclineChallenge(teamId, accept) {
  return profileStream.push(['ACCEPT_OR_DECLINE_CHALLENGE', { teamId, accept }])
}
