import { mergeDeepRight, compose, concat, uniq, without } from 'ramda'
import { createReducer } from '../../util'
import initialProfile from './data'

const updateProfile = (state, payload) => mergeDeepRight(state, payload)

const challengeTeam = (state, payload) => {
  const addOnlyOnce = compose(uniq, concat)
  const challenges = addOnlyOnce(state.challenges, [payload])
  return mergeDeepRight(state, { challenges })
}

const acceptDeclineChallenge = (state, payload) => {
  const challenges = without(payload.teamId, state.challenges)
  const addOnlyOnce = compose(uniq, concat)
  let matches = state.matches

  if (payload.accept) {
    matches = addOnlyOnce(state.matches, [payload.teamId])
  }

  return mergeDeepRight(state, { challenges, matches })
}

export const profileReducers = createReducer(initialProfile, {
  'UPDATE_PROFILE': updateProfile,
  'CHALLENGE_TEAM': challengeTeam,
  'ACCEPT_OR_DECLINE_CHALLENGE': acceptDeclineChallenge
})
