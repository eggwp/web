import erre from 'erre'
import store, { storeStream, getState } from '../../index'
import { profileReducers } from './reducer'

export const profileStream = erre(action => profileReducers(getState('profile'), action))
profileStream.on.value(profile => storeStream.push({ profile }))
