import { teamsStream } from './stream'

export function updateTeamAction(response) {
  return teamsStream.push(['UPDATE_TEAM', response])
}
