export default {
  1: {
    name: 'team2K',
    points: 2000,
    streak: 6
  },
  2: {
    name: 'Logitech.fi',
    points: 1800,
    streak: 4
  },
  3: {
    name: 'eSuba',
    points: 1700,
    streak: 0
  },
  4: {
    name: 'hxns',
    points: 1500,
    streak: 0
  }
}
