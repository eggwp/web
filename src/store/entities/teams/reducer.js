import { mergeDeepRight } from 'ramda'
import { createReducer } from '../../util'
import initialTeams from './data'

const updateTeam = (state, payload) => mergeDeepRight(state, payload)

export const teamsReducers = createReducer(initialTeams, {
  'UPDATE_TEAM': updateTeam
})
