export const createReducer = (initialState, handlers) =>
  (state = initialState, [action, payload]) => {
    if (handlers.hasOwnProperty(action)) {
      return handlers[action](state, payload)

    } else {
      return state
    }
  }
